package com.example.examenpractica;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class IngresoActivity extends AppCompatActivity {

    private TextView lblNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingreso);

        lblNombre = (TextView) findViewById(R.id.lblNombre);

        String nombre = "";

        Bundle bundle = getIntent().getExtras();
        nombre = bundle.getString("nombre");

        lblNombre.setText(nombre);

    }
}
