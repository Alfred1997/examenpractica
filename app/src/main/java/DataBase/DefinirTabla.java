package DataBase;

import android.provider.BaseColumns;

public class DefinirTabla {

    public DefinirTabla() {

    }

    public static abstract class Usuario implements BaseColumns {
        public static final String TABLA_NAME = "usuarios";
        public static final String NOMBRE = "nombre";
        public static final String USUARIO = "usuario";
        public static final String CONTRA = "contraseña";
    }

}
