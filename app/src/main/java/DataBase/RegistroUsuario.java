package DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import static android.icu.text.MessagePattern.ArgType.SELECT;

public class RegistroUsuario {
    private Context context;
    private RegistroDbHelper registroDbHelper;
    private SQLiteDatabase db;
    private String[] columnToRead = new String[] {
            DefinirTabla.Usuario._ID,
            DefinirTabla.Usuario.NOMBRE,
            DefinirTabla.Usuario.USUARIO,
            DefinirTabla.Usuario.CONTRA,
    };
    public RegistroUsuario(Context context){
        this.context = context;
        this.registroDbHelper= new RegistroDbHelper(this.context);
    }
    public void openDatabase() { db = registroDbHelper.getWritableDatabase(); }

    public long insertarUsuario(Usuario u){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Usuario.NOMBRE, u.getNombre());
        values.put(DefinirTabla.Usuario.USUARIO, u.getUsuario());
        values.put(DefinirTabla.Usuario.CONTRA, u.getContra());

        return db.insert(DefinirTabla.Usuario.TABLA_NAME, null, values);
    }
    public long actualizarUsuario(Usuario u, long id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Usuario.NOMBRE, u.getNombre());
        values.put(DefinirTabla.Usuario.USUARIO, u.getUsuario());
        values.put(DefinirTabla.Usuario.CONTRA, u.getContra());

        String criterio = DefinirTabla.Usuario._ID + " = " + id;

        return db.update(DefinirTabla.Usuario.TABLA_NAME, values, criterio, null);
    }
    public long eliminarUsuario(long id){
        String criterio = DefinirTabla.Usuario._ID + " = " + id;

        return db.delete(DefinirTabla.Usuario.TABLA_NAME, criterio, null);
    }

    public Usuario leerUsuario(Cursor cursor){
        Usuario u = new Usuario();

        u.set_ID(cursor.getInt(0));
        u.setNombre(cursor.getString(1));
        u.setUsuario(cursor.getString(2));
        u.setContra(cursor.getString(3));

        return u;
    }
    public Usuario getUsuario(long id){
        Usuario usuario = null;
        SQLiteDatabase db = this.registroDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Usuario.TABLA_NAME, columnToRead,
                DefinirTabla.Usuario._ID + " = ? ",
                new String[] { String.valueOf(id) },
                null, null, null);

        if(c.moveToFirst()){
            usuario = leerUsuario(c);
        }
        c.close();
        return usuario;
    }

    public Usuario getUser(String u, String c){
        Usuario usuario = null;
        SQLiteDatabase db = this.registroDbHelper.getReadableDatabase();
        Cursor cursor = db.query(DefinirTabla.Usuario.TABLA_NAME, columnToRead,
                DefinirTabla.Usuario.USUARIO + " = ? AND " +
                        DefinirTabla.Usuario.CONTRA + " = ? ",
                new String[] { String.valueOf(u), String.valueOf(c) },
                null, null, null);

        if(cursor.moveToFirst()){
            usuario = leerUsuario(cursor);
        }
        cursor.close();
        return usuario;
    }

    public ArrayList<Usuario> allUsuarios() {
        ArrayList<Usuario> usuarios = new ArrayList<>();
        Cursor cursor = db.query(DefinirTabla.Usuario.TABLA_NAME,
                null, null, null,
                null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Usuario u = leerUsuario(cursor);
            usuarios.add(u);
            cursor.moveToNext();
        }

        cursor.close();
        return usuarios;
    }

    public void cerrar() { registroDbHelper.close(); }
}
